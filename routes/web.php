<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::middleware(['auth' ,'CheckRole:admin'])->group(function () {
    Route::get('/', 'AdminController@index');

    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/{id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{id}', 'KategoriController@update');
    Route::delete('/kategori/{id}', 'KategoriController@destroy');

    //Crud Product

    Route::get('/product', 'ProductController@index');
    Route::get('/product/create', 'ProductController@create');
    Route::post('/product', 'ProductController@store');
    Route::get('/product/{id}/edit', 'ProductController@edit');
    Route::put('/product/{id}', 'ProductController@update');
    Route::delete('/product/{id}', 'ProductController@destroy');
    Route::get('/product/{id}', 'ProductController@show');
    
    
    //CRUD untuk profile
    
    Route::get('/profil/create', 'ProfileController@create');
    Route::get('/profil', 'ProfileController@index');
    Route::post('/profil', 'ProfileController@store');
    Route::get('/profil/{profil_id}', 'ProfileController@show');
    Route::get('/profil/{profil_id}/edit', 'ProfileController@edit');
    Route::put('/profil/{profil_id}', 'ProfileController@update');
    Route::delete('/profil/{profil_id)', 'ProfileController@destroy');
    
    
    //CRUD untuk Users
    
    Route::get('/user/create', 'UserController@create');
    Route::get('/user', 'UserController@index');
    Route::post('/user', 'UserController@store');
    Route::get('/user/{user_id}', 'UserController@show');
    Route::get('/user/{user_id}/edit', 'UserController@edit');
    Route::put('/user/{user_id}', 'UserController@update');
    Route::delete('/user/{user_id)', 'UserController@destroy');
});

//CRUD untuk profile

Route::get('/profil/create', 'ProfileController@create');
Route::get('/profil', 'ProfileController@index');
Route::post('/profil', 'ProfileController@store');
Route::get('/profil/{profil_id}', 'ProfileController@show');
Route::get('/profil/{profil_id}/edit', 'ProfileController@edit');
Route::put('/profil/{profil_id}', 'ProfileController@update');
Route::delete('/profil/{profil_id)', 'ProfileController@destroy');


//CRUD untuk Users

Route::get('/user/create', 'UserController@create');
Route::get('/user', 'UserController@index');
Route::post('/user', 'UserController@store');
Route::get('/user/{user_id}', 'UserController@show');
Route::get('/user/{user_id}/edit', 'UserController@edit');
Route::put('/user/{user_id}', 'UserController@update');
Route::delete('/user/{user_id)', 'UserController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
