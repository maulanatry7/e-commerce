<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    protected $fillable = ["id","name", "file_path", "created_at", "updated_at"];

    public function product(){
        return $this->hasMany('App\Product');
    }
}
