<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    protected $table = "profil";
    protected $fillable = ["nama", "no_hp", "alamat", "user_id"];
}
