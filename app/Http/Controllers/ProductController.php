<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use DB;

class ProductController extends Controller
{
    public function index(){
        $product = DB::table('product')->get();
        //dd($cast);
        return view('product.index', compact('product'));
    }

    public function create(){
        $kategori = DB::table('kategori')->get();
        //dd($cast);
        return view('product.create', compact('kategori'));
    }

    public function store(Request $request){
        $request->validate([
            'kategori' => 'required',
            'nama' => 'required',
            'desk' => 'required',
            'stock' => 'required',
            'harga' => 'required'
        ]);
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        $query = DB::table('product')->insert([
            "nama" => $request["nama"],
            "deskripsi" => $request["desk"],
            "stock" => $request["stock"],
            "harga" => $request["harga"],
            "kategori_id" => $request["kategori"],
            "image"=>$imageName
        ]);
        return redirect('/product')->with('success', 'Product berhasil di simpan !');
    }

    public function edit($id){
        $product=DB::table('product')
        ->where('id',$id)->first();
        $kategori = DB::table('kategori')->get();
        return view('product.edit', compact('product','kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'kategori' => 'required',
            'nama' => 'required',
            'desk' => 'required',
            'stock' => 'required',
            'harga' => 'required'
        ]);
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        $query = DB::table('product')
                    ->where('id', $id)
                    ->update([
                        "nama" => $request["nama"],
                        "deskripsi" => $request["desk"],
                        "stock" => $request["stock"],
                        "harga" => $request["harga"],
                        "kategori_id" => $request["kategori"],
                        "image"=>$imageName
                    ]);
        return redirect('/product')->with('success', 'product berhasil di update !');
    }

    public function destroy($id){
        $query = DB::table('product')->where('id', $id)->delete();
        return redirect('/product')->with('success', 'product berhasil di hapus !');
    }

    public function show($id){
        $product=DB::table('product')
        ->where('id',$id)->first();
        $kategori = DB::table('kategori')->get();
        return view('product.show', compact('product','kategori'));   
    }
}
