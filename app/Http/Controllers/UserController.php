<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function create(){
        return view('user.create');
    }

    public function store(Request $request){
        $request->validate([
            'username'=>'required',
            'password'=>'required',
            'email'=>'required'
        ]);

        $query = DB::table('users')->insert([
            'username' =>$request["username"],
            'password'=>$request["password"],
            'email'=>$request["email"]
        ]);
        return redirect('/user');
    }
    public function index(){
        $user = DB::table('product')->get();
        return view('user.index', compact('user'));
    }

    public function show($id){
        $user = DB::table('product')->where('id', $id)->first();
        return view('user.show', compact('user'));
    }

    public function edit($id){
        $user = DB::table('users')->where('id', $id)->first();
        return view('user.edit', compact('user'));
    }

    public function update($id, Request $request){
        $request->validate([
            'username'=>'required',
            'password'=>'required',
            'email'=>'required'
        ]);

        $query = DB::table('users')
        ->where('id', $id)
        ->update([
            'username' =>$request["username"],
            'password'=>$request["password"],
            'email'=>$request["email"]
        ]);
        return redirect('/user');
        }
    
        public function destroy($id){
            $query = DB::table('users')->where('id', $id)->delete();
            return redirect('/user');
        }
}
