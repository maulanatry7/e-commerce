<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    public function index(){
        $transaksi = DB::table('transaksi')->get();
        //dd($cast);
        return view('admin.dashboard', compact('transaksi'));
    }
}
