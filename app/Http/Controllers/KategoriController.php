<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class KategoriController extends Controller
{
    public function index(){
        $kategori = DB::table('kategori')->get();
        //dd($cast);
        return view('kategori.index', compact('kategori'));
    }
    public function create(){
        return view('kategori.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);
        $query = DB::table('kategori')->insert([
            "nama" => $request["nama"]
        ]);
        return redirect('/kategori')->with('success', 'Kategori berhasil di simpan !');
    }

    public function edit($id){
        $kategori=DB::table('kategori')->where('id',$id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ]);
        $query = DB::table('kategori')
                    ->where('id', $id)
                    ->update([
                    'nama' => $request['nama']
                    ]);
        return redirect('/kategori')->with('success', 'kategori berhasil di update !');
    }

    public function destroy($id){
        $query = DB::table('kategori')->where('id', $id)->delete();
        return redirect('/kategori')->with('success', 'kategori berhasil di hapus !');
    }
}
