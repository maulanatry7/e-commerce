<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function create(){
        return view('profil.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'user_id'=>'required'
        ]);

        $query = DB::table('profil')->insert([
            'nama' =>$request["nama"],
            'no_hp'=>$request["no_hp"],
            'alamat'=>$request["alamat"],
            'user_id'=>$request["user_id"]
        ]);
        return redirect('/profil');
    }
    public function index(){
        $profil = DB::table('profil')->get();
        return view('profil.index', compact('profil'));
    }

    public function show($id){
        $profil = DB::table('profil')->where('id', $id)->first();
        return view('profil.show', compact('profil'));
    }

    public function edit($id){
        $profil = DB::table('profil')->where('id', $id)->first();
        return view('profil.edit', compact('profil'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'user_id'=>'required'
        ]);

        $query = DB::table('profil')
        ->where('id', $id)
        ->update([
            'nama' =>$request["nama"],
            'no_hp'=>$request["no_hp"],
            'alamat'=>$request["alamat"],
            'user_id'=>$request["user_id"]
        ]);
        return redirect('/profil');
        }
    
        public function destroy($id){
            $query = DB::table('profil')->where('id', $id)->delete();
            return redirect('/profil');
        } 
}
