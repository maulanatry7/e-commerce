<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "product";
    protected $fillable = ["name", "file_path", "created_at", "updated_at"];

    public function kategori(){
        return $this->belongsTo('app\kategori','kategori_id');
    }
}
