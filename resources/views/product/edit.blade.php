@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New kategori</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/product/{{$product->id}}" enctype="multipart/form-data" method="POST">
              @csrf
              @method('PUT')
                  <div class="card-body">
                    <div class="form-group">
                    <div class="form-group">
                      <label>Kategori</label>
                      <select class="custom-select" name="kategori">
                        <option>--pilih kategori--</option>
                      @forelse($kategori as $kategori)
                        <option value="{{$kategori->id}}">{{$kategori->nama}}</option>
                        @empty
                      @endforelse
                      </select>
                    </div>
                      <label>Nama</label>
                      <input type="text" name="nama" value="{{$product->nama}}" class="form-control">
                      @error('nama')
                      <div class="alert alert-danger">{{$message}}</div>
                      @enderror  
                    </div>
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <input type="text" name="desk" value="{{$product->deskripsi}}" class="form-control">
                      @error('desk')
                      <div class="alert alert-danger">{{$message}}</div>
                      @enderror  
                  </div>
                  <div class="form-group">
                      <label>Stock</label>
                      <input type="number" name="stock" value="{{$product->stock}}" class="form-control">
                      @error('stock')
                      <div class="alert alert-danger">{{$message}}</div>
                      @enderror  
                  </div>
                  <div class="form-group">
                      <label>Harga</label>
                      <input type="number" name="harga" value="{{$product->harga}}" class="form-control">
                      @error('harga')
                      <div class="alert alert-danger">{{$message}}</div>
                      @enderror  
                  </div>
                  <div class="form-group">
                      <label>Image</label>
                      <br>
                      <input type="file" value="{{$product->image}}" name="image">
                      @error('image')
                      <div class="alert alert-danger">{{$message}}</div>
                      @enderror  
                  </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
    </section>
@endsection