@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data product</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                {{session('success')}}
                </div>
              @endif
                <a href="/product/create" class="btn btn-primary mb-2">Create new product</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Jumlah Stock</th>
                      <th>Harga</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($product as $key => $product)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$product->nama}}</td>
                        <td>{{$product->stock}}</td>
                        <td>{{$product->harga}}</td>
                        <td style="display:flex;">
                            <a href="product/{{$product->id}}" class="btn btn-info btn-xs">Show</a>
                            <a href="product/{{$product->id}}/edit" class="btn btn-default btn-xs">Edit</a>
                            <form action="/product/{{$product->id}}" method="post">
                            @csrf
                            @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-xs" >
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center" >No Data product</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
        </div> 
    </section>
@endsection