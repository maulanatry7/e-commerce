@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
          <div class="card card-solid">
          <div class="card-body">
            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="col-12">
                  <img src="../images/{{$product->image}}" class="product-image" alt="Product Image">
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <h3 class="my-3">{{$product->nama}}</h3>
                <p>{{$product->deskripsi}}</p>

                <hr>
                <div class="bg-gray py-2 px-3 mt-4">
                  <h2 class="mb-0">
                  {{$product->harga}}
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        </div> 
    </section>
@endsection