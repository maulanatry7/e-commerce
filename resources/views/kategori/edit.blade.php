@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit kategori {{$kategori->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/kategori/{{$kategori->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" value="{{old('nama',$kategori->nama)}}" class="form-control">
                    @error('nama')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror  
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
    </section>
@endsection