@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New kategori</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/kategori" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control">
                    @error('nama')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror  
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
    </section>
@endsection