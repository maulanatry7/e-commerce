@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data kategori</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                {{session('success')}}
                </div>
              @endif
                <a href="/kategori/create" class="btn btn-primary mb-2">Create new kategori</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($kategori as $key => $kategori)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$kategori->nama}}</td>
                        <td style="display:flex;">
                            <a href="kategori/{{$kategori->id}}/edit" class="btn btn-default btn-xs">Edit</a>
                            <form action="/kategori/{{$kategori->id}}" method="post">
                            @csrf
                            @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-xs" >
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" align="center" >No Data kategori</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
        </div> 
    </section>
@endsection