@extends('layout.master')

@section('title')
Index Profile
@endsection

@section('content')
<a href="/profil/create" class="btn btn-primary">Buat Akun</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
               
                <th scope="col">Nama</th>
                <th scope="col">No HP</th>
                <th scope="col">Alamat</th>
                <th scope="col">User ID</th>
                
              </tr>
            </thead>
            <tbody>
                @forelse ($profil as $key=>$value)
                    <tr>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->no_hp}}</td>
                        <td>{{$value->alamat}}</td>
                        <td>{{$value->user_id}}</td>
                        <td>
                            <a href="/profil/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/profil/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/profil/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection