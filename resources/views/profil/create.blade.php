@extends('layout.master')

@section('title')
Profile
@endsection

@section('content')
<form action="/profil" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id= "nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="no_hp">No HP</label>
        <input type="text" class="form-control" name="no_hp" id= "no_hp" placeholder="Masukkan No. HP">
        @error('no_hp')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" name="alamat" id= "alamat" placeholder="Masukkan Alamat">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">User ID</label>
        <input type="text" class="form-control" name="user_id" id= "user_id" placeholder="Masukkan User ID">
        @error('user_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
   
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection