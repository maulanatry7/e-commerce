@extends('layout.master')
@section('title')
Edit Profile
@endsection

@section('content')
<h2>Tambah Data</h2>
        <form action="/profil/{{$profil->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" value="{{$profil->nama}}" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">No HP</label>
                <input type="text" class="form-control" name="no_hp" value="{{$profil->no_hp}}" placeholder="Masukkan Nomor HP">
                @error('no_hp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Alamat</label>
                <input type="text" class="form-control" name="alamat" value="{{$profil->alamat}}" placeholder="Masukkan Alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">User ID</label>
                <input type="text" class="form-control" name="user_id" value="{{$profil->user_id}}" placeholder="Masukkan User ID (angka)">
                @error('user_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection