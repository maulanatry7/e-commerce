@extends('layout.master')
@section('title')
Edit Profile
@endsection

@section('content')
<h2>Tambah Data</h2>
        <form action="/user/{{$user->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Username</label>
                <input type="text" class="form-control" value="{{$user->username}}" name="username" placeholder="Masukkan Username">
                @error('username')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Password</label>
                <input type="text" class="form-control" name="password" value="{{$user->password}}" placeholder="Masukkan Password">
                @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Email</label>
                <input type="text" class="form-control" name="email" value="{{$user->email}}" placeholder="Masukkan Alamat Email">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
           
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection