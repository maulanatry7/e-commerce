@extends('layout.master')

@section('title')
Users
@endsection

@section('content')
<form action="/user" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Username</label>
        <input type="text" class="form-control" name="username" id= "username" placeholder="Masukkan Username">
        @error('username')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="no_hp">Password</label>
        <input type="text" class="form-control" name="password" id= "password" placeholder="Masukkan Password">
        @error('password')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Email</label>
        <input type="text" class="form-control" name="email" id= "email" placeholder="Masukkan Email">
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
   
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection